import Axios from "axios";

export class AddressService {
    constructor() {
        this.url = 'http://localhost:8000/api/address'
    }

    async findAll() {
        let response = await Axios.get(this.url);
        return response.data;
    }
}