import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Register from "./views/Register.vue";
import MyAddresses from "./views/MyAddresses.vue";
import NotFound from "./views/NotFound.vue";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
     },
     {
       path: '/register',
       name:'register',
       component: Register
     },
     {
       path: '/my-addresses',
       name:'my-addresses',
       component: MyAddresses
     },
     {
       path:'**',
       name:'not-found',
       component: NotFound
     }
  ]
})
